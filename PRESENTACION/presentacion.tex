\documentclass{beamer}
\usetheme{Darmstadt}
\usecolortheme{beaver}

\usepackage{tabularx}
\usepackage{multicol}
\usepackage{multirow}
\usepackage[textfont={scriptsize,it}]{caption}
\usepackage{subcaption}

\usepackage[utf8]{inputenc}
\usepackage[english,spanish]{babel}
\selectlanguage{spanish}

\title{Estudio de la evolución de los derechos LGBT con aprendizaje automático}
\author[Ares Aguilar Sotos]{Ares Aguilar Sotos\\{\small Tutor: David Domínguez Carreta}}
\institute{Escuela Politécnica Superior\\{\small Universidad Autónoma de Madrid}}
\date{\today}

\titlegraphic{\includegraphics[width=2cm]{../MEMORIA/images/logo_eps.png}\hspace*{4.75cm}~%
   \includegraphics[width=2cm]{../MEMORIA/images/logo_uam.jpg}
}

\begin{document}

\titlepage

\begin{frame}{Tabla de contenidos}
  \begin{multicols}{2}
    \tableofcontents%[subsectionstyle=show]
  \end{multicols}
\end{frame}

\begin{section}{Introducción}
  \begin{subsection}{Motivación}
    \begin{frame}{Motivación}
      \begin{quote}<1->
        Ser homosexual sigue siendo delito en 75 países. \\
        \hfill (El Plural, Mayo 2016)
      \end{quote}
      \begin{quote}<2->
        La región [Madrid] sufre una agresión homófoba cada dos días.
        \hfill (El País, Abril 2016)
      \end{quote}
      \begin{quote}<3->
        Nueva agresión homófoba: insultan y patean a una transexual en Puente de Vallecas. \\
        \hfill (ABC, Febrero 2017)
      \end{quote}
    \end{frame}
  \end{subsection} % Motivación
  
  \begin{subsection} {Objetivos y alcance}
    \begin{frame} {Objetivos y alcance}
      \begin{itemize}
        \item <1-> Objetivos
        \begin{itemize}
          \item<2-> Estudiar la efectividad del aprendizaje automático en la predicción de series temporales
          \item<3-> Difundir la lucha por la igualdad de derechos LGBT
          \item<4-> Estudiar y predecir las tendencias mundiales en igualdad de derechos
        \end{itemize}
        \item<5-> Alcance
        \begin{itemize}
          \item<6-> Estudio de datos legales
          \item<7-> Estudio de los hombres homosexuales (excluyendo lesbianas, bisexuales, transexuales e intersexuales)
        \end{itemize}
      \end{itemize}
      \pause
      
    \end{frame}
  \end{subsection} % Objetivos y alcance

  \begin{subsection} {Tecnología y herramientas empleadas}
    \begin{frame} {Tecnología y herramientas empleadas}
      \begin{description}
      \item<1-> [R] Lenguaje de programación enfocado al análisis estadístico
      \item<2-> [Shiny] Framework de aplicaciones web para R
      \item<3-> [Weka] Plataforma para el aprendizaje automático y minería de datos
      \item<4-> [AWK] Lenguaje de programación para el procesamiento de datos basados en texto
      \item<5-> [Emacs] \textit{the extensible, customizable, self-documenting real-time display editor}
      \end{description}
    \end{frame}
  \end{subsection} % Tecnología y herramientas empleadas
\end{section} % Introducción


\begin{section}{Estado del arte}
  \begin{subsection} {Igualdad LGBT}
    \begin{frame} {Medidores de la igualdad LGBT}
      \begin{itemize}
      \item<1-> \textit{State-Sponsored Homophobia 2016: A world survey of sexual orientation laws: criminalisation, protection and recognition}, ILGA (2016)
      \item<2-> \textit{Legal recognition of homosexual orientation in the countries of the world. A chronological overview with footnotes}, Kees Waaldijk (2009)
      \item<3-> \textit{The Relationship between LGBT inclusion and Economic Development: An Analysis of Emerging Economies}, Kees Waaldijk (2014)
      \end{itemize}
    \end{frame}
    \begin{frame} {Leyes consideradas en el GILRHO}
      \begin{table}[]
        \centering
        \resizebox{\textwidth}{!}{
          \begin{tabularx}{400pt}{|l|X|}
            \hline
            \multirow{2}{*}{Descriminalización}    & Descriminalización de las relaciones sexuales entre personas del mismo sexo.\\ \cline{2-2}
                                                   & Igualdad de la edad legal de consentimiento entre relaciones sexuales homosexuales y heterosexuales. \\ \hline
            \multirow{2}{*}{Anti-discriminación}   & Prohibición explícita de la discriminación en el empleo por motivos de orientación sexual. \\ \cline{2-2}
                                                   & Prohibición de la discriminación en relación a bienes y servicios (incluyendo vivienda, asistencia sanitaria y educación) por motivos de orientación sexual. \\ \hline
            \multirow{2}{*}{Parejas homosexuales}  & Reconocimiento legal de la cohabitación no registrada entre personas del mismo sexo. \\ \cline{2-2}
                                                   & Reconocimiento legal de la unión civil o cohabitación registrada entre personas del mismo sexo, con los mismos derechos que las uniones heterosexuales. \\ \hline
            \multirow{2}{*}{Familias homosexuales} & Reconocimiento legal del matrimonio homosexual. \\ \cline{2-2}
                                                   & Permisión de la adopción por parte de personas y/o parejas homosexuales. \\ \hline
        \end{tabularx}}
      \end{table}
    \end{frame}
  \end{subsection} % Igualdad LGBT

  \begin{subsection} {Predicción de series temporales}
    \begin{frame} {Predicción de series temporales}
      \begin{itemize}
        \item \emph{Modelos autorregresivos}: Introducidos por Yule y Walker a principios del siglo XX
        \item \emph{Modelos Box-Jenkins}: Desarrollados en 1970, han dado lugar a los modelos ARCH y GARCH ampliamente utilizados hoy en día
        \item \emph{Redes neuronales}: En la actualidad han mejorado las predicciones de los anteriores
        \item \emph{Bagging}: Aplicado a meteorología, medicina y economía
      \end{itemize}
    \end{frame}
  \end{subsection} % Predicción de series temporales
\end{section} % Estado del arte

\begin{section} {Base de datos y algoritmos empleados}
  \begin{subsection} {Base de datos}
    \begin{frame} {Base de datos empleada}
      \begin{description}
        \item [Temporal] Se recoge el año de cada muestra
        \item [Geográfica] Cada país se etiqueta según el estándar ISO3166, y se incluyen su región y subregión geográfica según el estándar UN.M49
        \item [Legal] Para cada país y año, se calcula el GILRHO según las leyes que cumple
      \end{description}
    \end{frame}
  \end{subsection} % Base de datos

  \begin{subsection} {Algoritmos empleados}
    \begin{frame} {Algoritmos de regresión}
      \begin{multicols}{3}
        \centering
        Vecinos próximos
        \begin{figure}[htb]
          \centering
          \includegraphics[width=.95\linewidth]{../R/plots/errors_knn.png}
        \end{figure}
        \vfill
        \columnbreak
        \pause
        Selvas aleatorias
        \begin{figure}[htb]
          \centering
          \includegraphics[width=.95\linewidth]{../R/plots/errors_rf.png}
        \end{figure}
        \vfill
        \columnbreak
        \pause
        Redes neuronales
        \begin{figure}[htb]
          \centering
          \includegraphics[width=.95\linewidth]{../R/plots/errors_ann.png}
        \end{figure}
        \vfill
      \end{multicols}
    \end{frame} % Algoritmos de regresión
    \begin{frame} {Algoritmos de clasificación}
      Clustering jerárquico (UPGMA) con distorsión temporal dinámica (DTW)
      \begin{figure}[htb]
          \centering
          \includegraphics[width=.9\linewidth]{../R/plots/cluster.png}
      \end{figure}
    \end{frame}
  \end{subsection} % Algoritmos empleados
\end{section} % Base de datos y algoritmos empleados

\begin{section} {Estudio de la igualdad de derechos}
  \begin{subsection} {Estudio de la serie temporal}
    \begin{frame} {Estudio de la serie temporal}
      \begin{figure}
         \begin{subfigure}{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_knn_2025.png}
           \caption{Vecinos próximos (2025)}
         \end{subfigure} \\
         \begin{subfigure}[b]{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_rf_2025.png}
           \caption{Selvas aleatorias (2025)}
         \end{subfigure}
         \begin{subfigure}[b]{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_ann_2025.png}
           \caption{Red neuronal (2025)}
         \end{subfigure}
       \end{figure}
     \end{frame}
  \end{subsection} % Estudio de la serie temporal
  \begin{subsection} {Estudio de escenarios}
    \begin{frame} {Estudio de escenarios}
      \begin{figure}
         \begin{subfigure}{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_ann_2025.png}
           \caption{Escenario normal (2025)}
         \end{subfigure} \\
         \begin{subfigure}[b]{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_ann_2025_pes.png}
           \caption{Escenario pesimista (2025)}
         \end{subfigure}
         \begin{subfigure}[b]{0.4\textwidth}
           \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/gilrho_ann_2025_opt.png}
           \caption{Escenario optimista (2025)}
         \end{subfigure}
       \end{figure}
    \end{frame}
  \end{subsection} % Estudio de escenarios
  \begin{subsection} {Estudio de pioneros y seguidores}
    \begin{frame} {Estudio de pioneros y seguidores}
      \begin{figure}
        \begin{subfigure}{0.49\textwidth}
          \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/cluster_2.png}
          \caption{Clustering de 2 agrupaciones}
        \end{subfigure}
        \begin{subfigure}{0.49\textwidth}
          \includegraphics[width=\textwidth]{../MEMORIA/IMAGENES/cluster_5.png}
          \caption{Clustering de 5 agrupaciones}
        \end{subfigure}
      \end{figure}
    \end{frame}
  \end{subsection} % Estudio de pioneros y seguidores
\end{section} % Estudio de la igualdad de derechos de los homosexuales

\begin{section} {Conclusiones}
  \begin{frame} {Conclusiones y trabajo futuro}
    \begin{itemize}
      \item<1-> Los algoritmos de regresión estudiados igualan las técnicas ARMA y ARIMA
      \item<1-> Los algoritmos de clasificación permiten estudiar pioneros y seguidores
      \item<2-> Los Estados que criminalizan la homosexualidad no proyectan mejoras en el futuro
      \item<2-> América Latina muestra un futuro esperanzador en términos de igualdad LGBT \\
      \item<3-> Publicaciones en revistas sociales, difusión de los mapas a través de organizaciones LGBT
    \end{itemize}
  \end{frame}
  \begin{frame}
    \begin{quote}
        A prisión los agresores de un homosexual en Fomento al rechazar la Audiencia su recurso \\
        \hfill (La Nueva España, Enero 2017)
      \end{quote}
  \end{frame}
\end{section} % Conclusiones y trabajo futuro
\end{document}