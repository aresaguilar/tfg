function(input, output, session) {
  
  points <- eventReactive(input$recalc, {
    cbind(rnorm(40) * 2 + 13, rnorm(40) + 48)
  }, ignoreNULL = FALSE)
  
  output$mymap <- renderLeaflet({
    leaflet(data = map) %>%
      addTiles(options = tileOptions(continuousWorld = FALSE,
                                     #noWrap = TRUE,
                                     reuseTiles = TRUE,
                                     minZoom = 1,
                                     maxZoom = 6)) %>%
      #addProviderTiles("CartoDB.Positron") %>%
      #addProviderTiles("Stamen.Watercolor") %>%
      addPolygons(stroke = FALSE, layerId = lay1,
                  fillColor = ~palknn(knn), opacity=1, fillOpacity=0.7,
                  group = "kNN", popup = gilrho_popup) %>%
      addPolygons(stroke = FALSE, layerId = lay2,
                  fillColor = ~palknn(rf), opacity=1, fillOpacity=0.7,
                  group = "Random Forest", popup = gilrho_popup) %>%
      addPolygons(stroke = FALSE, layerId = lay3,
                  fillColor = ~palknn(ann), opacity=1, fillOpacity=0.7,
                  group = "Neural Network", popup = gilrho_popup) %>%
      addLegend("bottomleft", pal = palknn, values = ~knn,
                title = "GILRHO",
                opacity = 1) %>%
      addLayersControl(
        baseGroups = c("kNN", "Random Forest", "Neural Network"),
        position = "topleft",
        options = layersControlOptions(collapsed = FALSE)
      ) %>%
      setView(lng = 0, lat = 28, zoom = 2)
  })
  
  output$clustermap <- renderLeaflet({
    leaflet(data = mapCluster) %>%
      addTiles(options = tileOptions(continuousWorld = FALSE,
                                     #noWrap = TRUE,
                                     reuseTiles = TRUE,
                                     minZoom = 1,
                                     maxZoom = 6)) %>%
      #addProviderTiles("CartoDB.Positron") %>%
      #addProviderTiles("Stamen.Watercolor") %>%
      addPolygons(stroke = FALSE,
                  fillColor = ~palcluster(mcluster), opacity=1, fillOpacity=0.38,
                  group = "cluster", popup = country_popup) %>%
      setView(lng = 0, lat = 28, zoom = 2)
  })
  
  output$mytable = renderDataTable({
    tabla_knn
  },
  options = list(
    autoWidth = TRUE,
    columnDefs = list(list(width = '70px', targets = "_all"))
  ))
  
  observe({
    tab <- input$nav
    yearBy <- input$year
    nCluster <- input$clusters
    
    if (tab == "cluster") {
        
        mapCluster$mcluster <- mapCluster$cluster[[nCluster]]
        leafletProxy("clustermap", data = mapCluster) %>%
            clearGroup("cluster") %>%
            addPolygons(stroke = FALSE,
                        fillColor = ~palcluster(mcluster), opacity=1, fillOpacity=0.8,
                        group = "cluster", popup = country_popup)
        
    } else if (tab == "mapa") { 
        
        map$knn <- map$GILRHOknn[[yearBy]]
        map$rf  <- map$GILRHOrf[[yearBy]]
        map$ann <- map$GILRHOann[[yearBy]]
        
        if (yearBy < 2017) {
          gilrho_popup <- paste0("<center><strong>", map$name, "</strong></center>",
                                 "<strong>GILRHO: </strong>", 
                                 format(round(map$knn,2), nsmall = 1))
        } else {
          gilrho_popup <- paste0("<center><strong>", map$name, "</strong></center>",
                                 "<strong>GILRHO (kNN): </strong>", 
                                 format(round(map$knn,2), nsmall = 1), 
                                 "<br><strong>GILRHO (Random Forest): </strong>", 
                                 format(round(map$rf,2), nsmall = 1), 
                                 "<br><strong>GILRHO (Neural Network): </strong>", 
                                 format(round(map$ann,2), nsmall = 1))
        }
        var <- FALSE
        if (yearBy > 2016) {
          var <- TRUE
        }
        if (var) {
          leafletProxy("mymap", data = map) %>%
            addPolygons(stroke = FALSE, layerId = lay1,
                        fillColor = ~palknn(knn), opacity=1, fillOpacity=0.7,
                        group = "kNN", popup = gilrho_popup) %>%
            addPolygons(stroke = FALSE, layerId = lay2,
                        fillColor = ~palknn(rf), opacity=1, fillOpacity=0.7,
                        group = "Random Forest", popup = gilrho_popup) %>%
            addPolygons(stroke = FALSE, layerId = lay3,
                        fillColor = ~palknn(ann), opacity=1, fillOpacity=0.7,
                        group = "Neural Network", popup = gilrho_popup) %>%
            clearGroup("GILRHO")%>%
            addLayersControl(
              baseGroups = c("kNN", "Random Forest", "Neural Network"),
              position = "topleft",
              options = layersControlOptions(collapsed = FALSE)
            )
        } else {
          leafletProxy("mymap", data = map) %>%
            addPolygons(stroke = FALSE, layerId = lay1,
                        fillColor = ~palknn(knn), opacity=1, fillOpacity=0.7,
                        group = "GILRHO", popup = gilrho_popup) %>%
            clearGroup("kNN") %>%
            clearGroup("Random Forest") %>%
            clearGroup("Neural Network")
        }
        
    }
    
    })
}