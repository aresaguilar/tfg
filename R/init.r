######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: init.r                                                       #
# Description: Init routine that sets the proper working directory,  #
#              cheks if all packages are installed and loads         #
#              required modules.                                     #
# Author: Ares Aguilar Sotos                                         #
######################################################################

######################################################################
# Function name: require-or-install                                  #
# Description: Tries to load a package and installs it if not found  #
# Arguments: - PACKAGE : Name of the package to load                 #
######################################################################
require_or_install <- function(PACKAGE) {
    if (!require(PACKAGE,character.only = TRUE)) {
      install.packages(PACKAGE,dep=TRUE)
      if(!require(PACKAGE,character.only = TRUE))
          stop("Package not found")
    }
  }

# Set working directory
setwd("~/TFG/R")

# Check and install required packages
required_packages = c("XLConnect",     # Working with Excel files
                      "ISOcodes",      # Region codes
                      "RColorBrewer",  # Color palette for the map
                      "R.utils",       # Various R utilities
                      "devtools",      # For github install
                      "quantmod",
                      "matrixStats",
                      "dplyr",
                      "RWeka",
                      "shiny",
                      "ggplot2",
                      "ggdendro",
                      "leaflet",
                      "maps",
                      "reshape",
                      "dtw"
                      )
for (pckg in required_packages) {
    require_or_install(pckg)
}

## # Dependency for rMaps
## install_github('ramnathv/rCharts')
## require(rCharts)

## # Install my version of rMaps
## install_github('aresaguilar/rMaps')
## require(rMaps)
