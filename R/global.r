setwd("~/TFG/R")
source("init.r")
source("func.r")
source("func_cluster.r")

map <- load_data_map("output/pred_knn.csv", "output/pred_rf.csv", "output/pred_ann.csv")
# map <- load_data_map("output/pred_knn_opt.csv", "output/pred_rf_opt.csv", "output/pred_ann_opt.csv")
# map <- load_data_map("output/pred_knn_pes.csv", "output/pred_rf_pes.csv", "output/pred_ann_pes.csv")

mapCluster <- load_cluster_map("output/pred_cluster.csv")

tmp_knn <- read.csv("output/pred_knn.csv", header = TRUE)
tmp_knn$GILRHO <- format(round(tmp_knn$GILRHO,1), nsmall = 1)
tabla_knn <- cast(tmp_knn[tmp_knn$Years < 2017,], ISO3166~Years, value = "GILRHO")

year <- 2030
nCluster <- 10

lay1 <- as.character(c(1:1627))
lay2 <- as.character(c(1627:3254))
lay3 <- as.character(c(3254:4881))

# Subset year
map$knn <- map$GILRHOknn[[year]]
map$rf  <- map$GILRHOrf[[year]]
map$ann <- map$GILRHOann[[year]]
mapCluster$mcluster <- mapCluster$cluster[[nCluster]]

# Create palettes
palknn     <- colorBin("RdYlGn", map$knn, 8, pretty = FALSE, alpha = FALSE)
palrf      <- colorBin("RdYlGn", map$rf, 8, pretty = FALSE, alpha = FALSE)
palann     <- colorBin("RdYlGn", map$ann, 8, pretty = FALSE, alpha = FALSE)
palcluster <- colorFactor(rainbow(10), mapCluster$mcluster, alpha = FALSE)

# Create popup
gilrho_popup <- paste0("<center><strong>", map$name, "</strong></center>",
                       "<strong>GILRHO (kNN): </strong>", 
                       format(round(map$knn,2), nsmall = 1), 
                       "<br><strong>GILRHO (Random Forest): </strong>", 
                       format(round(map$rf,2), nsmall = 1), 
                       "<br><strong>GILRHO (Neural Network): </strong>", 
                       format(round(map$ann,2), nsmall = 1))
country_popup <- paste0("<center><strong>", map$name, "</strong></center>")