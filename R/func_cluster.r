######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: func_cluster.r                                               #
# Description: Functions to create and evaluate a hierarchical       #
#              clusterer for the LGBT data.                          #
# Author: Ares Aguilar Sotos                                         #
######################################################################

cluster_create <- function(df_cluster, mtd) {
    # Read file
    sc <- cast(df_cluster, ISO3166 ~ Years, value = 'GILRHO')
    data <- sc[2:58]
    # Compute distances
    distMatrix <- dist(data, method=mtd)
    # Create cluster
    hc <- hclust(distMatrix, method="average")
    # Write labels
    hc$labels <- sc$ISO3166
#   ggdendrogram(hc, rotate = FALSE, size = 2 )+ ggtitle("Cluster jerárquico")
    return(hc)
}


cluster_plot <- function(hc, filename) {
    ggdendrogram(hc, rotate = FALSE, size = 2 )+ ggtitle("Cluster jerárquico")
    ggsave(filename, scale = c(4,1.7))
}


cluster_predict <- function(hc, start, end) {
    data_cluster <- as.data.frame(hc$labels)
    names(data_cluster) <- c("ISO3166")
    for(i in start:end) {
        data_cluster <- cbind(data_cluster,cutree(hc, k = i ))
        names(data_cluster)[(i+1)] = c(as.character(i))
    }
    return(data_cluster)
}

load_cluster_map <- function(fcluster) {
  dataCluster <- read.csv(file = fcluster, header = TRUE)
  dataCluster$names <- iso.expand(lapply(dataCluster$ISO3166, as.character))
  
  mapWorld = map('world', fill = TRUE, col = 1:10, plot = FALSE)
  df_maps <- data.frame(mapWorld$names)
  df_maps$mapWorld.names <- as.character(df_maps$mapWorld.names)
  
  list_clust <- list()

  for (nclust in 1:10) {
    df_nclust <- data.frame(dataCluster$names,dataCluster[nclust+1])
    names(df_nclust)<-c("names","cluster")
    
    df_maps$cluster <- NA

    
    for(i in 1:nrow(df_nclust)) {
      df_maps$cluster[grepl(df_nclust$names[i], df_maps$mapWorld.names, perl = TRUE)] <- df_nclust$cluster[i]
      
    }
    df_maps$cluster <- as.numeric(as.character(df_maps$cluster))

    list_clust[[nclust]]  <- df_maps$cluster

  }
  mapWorld$cluster <- list_clust

  return(mapWorld)
}

display_cluster_map <- function(map, nCluster) {
  
  # Subset year
  map$mcluster <- map$cluster[[nCluster]]
  
  # Create palettes
  palcluster <- colorFactor(rainbow(nCluster), map$mcluster, alpha = FALSE)
  
  # Create leaflet
  leaflet(data = map) %>%
    addTiles() %>%
    #addProviderTiles("CartoDB.Positron") %>%
    #addProviderTiles("Stamen.Watercolor") %>%
    addPolygons(stroke = FALSE,
                fillColor = ~palcluster(mcluster), opacity=1, fillOpacity=0.9, group = "cluster")
}

