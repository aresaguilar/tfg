######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: prep.r                                                       #
# Description: Loads every data necessary, creating these variables: #
#               - ISO_3166_1: ISO codes for countries                #
#               - UN_M.49_Regions: M49 codes for regions             #
#               - df_gay: Gay rights dataframe                       #
#               - df_train: Timeseries with GILRHO                   #
#               - df_pred: Timeseries without GILRHO                 #
# Author: Ares Aguilar Sotos                                         #
######################################################################

# Load UN region data from ISOcodes package
data("ISO_3166_1")
data("UN_M.49_Regions")

# Read XLS
wb_gay <<- loadWorkbook("data/TimesSeriesData_50yearsofGILRHO.xls", create = FALSE)

# Initialize dataframe
df_gay <- data.frame(Years=integer(),
                     GILRHO=integer(),
                     ISO3166=character(),
                     M49_Subregion=character(),
                     M49_Region=character(),
                     Decade=character(),
                     stringsAsFactors=FALSE)

# Read sheets
for (sheet in getSheets(wb_gay)[-1]) {
  print(sheet)
  lst <- readWorksheet(wb_gay, sheet = sheet)
  df_sheet <- subset(lst, select = c(Years, GILRHO))
  
  # Append ISO3166
  iso3166_sheet <- ISO_3166_1$Alpha_3[(ISO_3166_1$Name == sheet)]
  df_sheet$ISO3166 <- rep(iso3166_sheet, nrow(df_sheet))
  m49_sheet <- ISO_3166_1$Numeric[(ISO_3166_1$Name == sheet)]
  
  # Append M49 Subregion
  subregion_sheet <- UN_M.49_Regions$Code[grepl(m49_sheet, UN_M.49_Regions$Children)]
  if (is.vector(subregion_sheet)) {
    subregion_sheet <- subregion_sheet[1]
  }
  df_sheet$M49_Subregion <- rep(subregion_sheet, nrow(df_sheet))
  
  # Append M49 Region
  region_sheet <- UN_M.49_Regions$Parent[grepl(m49_sheet, UN_M.49_Regions$Children)]
  if (is.vector(region_sheet)) {
    region_sheet <- region_sheet[1]
  }
  df_sheet$M49_Region <- rep(region_sheet, nrow(df_sheet))
  
  
  # Create predictions
  predictions_sheet <- data.frame(seq(2017, 2050), rep('?', 34),
                                  rep(iso3166_sheet, 34),
                                  rep(subregion_sheet, 34),
                                  rep(region_sheet, 34))
  colnames(predictions_sheet) <- colnames(df_sheet)
  df_sheet <- rbind(df_sheet, predictions_sheet)
  
  # Append decade number
  decade_sheet <- intToBin(((df_sheet$Years - (df_sheet$Years %% 10)) / 10) - 196)
  df_sheet$Decade <- decade_sheet
  
  # Store sheet in df_gay
  df_gay <- rbind(df_gay, df_sheet)
}
df_gay$Years<-as.integer(df_gay$Years)
# We won't use the Workbook anymore
remove(wb_gay)
remove(df_sheet)
remove(predictions_sheet)
remove(lst)

# Set Region means
df_gay$GILRHO <- as.numeric(df_gay$GILRHO)
df_gay$mean_region_prev1 <- 0.0
df_gay$mean_region_prev2 <- 0.0
for (region in unique(df_gay$M49_Region)) {
  for (year in 1960:2016) {
    mean <- mean(df_gay[((df_gay$Years == year) & (df_gay$M49_Region == region)), 'GILRHO'])
    df_gay[((df_gay$Years == year + 1) & (df_gay$M49_Region == region)), 'mean_region_prev1'] <- mean
    df_gay[((df_gay$Years == year + 2) & (df_gay$M49_Region == region)), 'mean_region_prev2'] <- mean
  }
}

# Set Subregion means
df_gay$mean_subregion_prev1 <- 0.0
df_gay$mean_subregion_prev2 <- 0.0
for (region in unique(df_gay$M49_Subregion)) {
  for (year in 1960:2016) {
    mean <- mean(df_gay[((df_gay$Years == year) & (df_gay$M49_Subregion == region)), 'GILRHO'])
    df_gay[((df_gay$Years == year + 1) & (df_gay$M49_Subregion == region)), 'mean_subregion_prev1'] <- mean
    df_gay[((df_gay$Years == year + 2) & (df_gay$M49_Subregion == region)), 'mean_subregion_prev2'] <- mean
  }
}

# Set Previous GILRHOs
df_gay$gilrho_prev1 <- 0.0
df_gay$gilrho_prev2 <- 0.0
df_gay$gilrho_prev3 <- 0.0
df_gay$gilrho_prev4 <- 0.0
df_gay$gilrho_prev5 <- 0.0
for (region in unique(df_gay$ISO3166)) {
  for (year in 1960:2016) {
    mean <- mean(df_gay[((df_gay$Years == year) & (df_gay$ISO3166 == region)), 'GILRHO'])
    df_gay[((df_gay$Years == year + 1) & (df_gay$ISO3166 == region)), 'gilrho_prev1'] <- mean
    df_gay[((df_gay$Years == year + 2) & (df_gay$ISO3166 == region)), 'gilrho_prev2'] <- mean
    df_gay[((df_gay$Years == year + 3) & (df_gay$ISO3166 == region)), 'gilrho_prev3'] <- mean
    df_gay[((df_gay$Years == year + 4) & (df_gay$ISO3166 == region)), 'gilrho_prev4'] <- mean
    df_gay[((df_gay$Years == year + 5) & (df_gay$ISO3166 == region)), 'gilrho_prev5'] <- mean
    
    }
}

# Get Data
df_train               <- subset(df_gay, !is.na(df_gay$GILRHO))
df_train$ISO3166       <- as.factor(df_train$ISO3166)
df_train$Decade        <- NULL #as.numeric(df_train$Decade)
df_train$GILRHO        <- as.numeric(df_train$GILRHO)
df_train$mean_subregion_diff <- (df_train$mean_subregion_prev1 - df_train$mean_subregion_prev2)
df_train$mean_region_diff <- (df_train$mean_region_prev1 - df_train$mean_region_prev2)

# Get Predictions   
df_pred             <- subset(df_gay, is.na(df_gay$GILRHO))
df_pred$ISO3166     <- as.factor(df_pred$ISO3166)
df_pred$Decade      <- NULL #as.numeric(df_pred$Decade)
df_pred$GILRHO      <- as.numeric(df_pred$GILRHO)
df_pred$mean_subregion_diff <- NULL
df_pred$mean_region_diff <- NULL

df_train_bak           <-df_train
df_train$M49_Region    <-as.factor(df_train$M49_Region)
df_train$M49_Subregion <-as.factor(df_train$M49_Subregion)
