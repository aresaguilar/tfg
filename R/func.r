######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: func.r                                                       #
# Description: Generic functions to work with the LGBT data.         #
# Author: Ares Aguilar Sotos                                         #
######################################################################

stand <- function(x) {
    if (x < 0)
        return(0)
    if (x > 8)
        return (8)
    return (x)
}

update_means <- function(df_pred, year) {
    # Update Region
    for (region in unique(df_pred$M49_Region)) {
        mean <- mean(df_pred[((df_pred$Years == year) & (df_pred$M49_Region == region)), 'GILRHO'])
        df_pred[((df_pred$Years == year + 1) & (df_pred$M49_Region == region)), 'mean_region_prev1'] <- mean
        df_pred[((df_pred$Years == year + 2) & (df_pred$M49_Region == region)), 'mean_region_prev2'] <- mean
    }

    # Update Subregion
    for (region in unique(df_pred$M49_Subregion)) {
        mean <- mean(df_pred[((df_pred$Years == year) & (df_pred$M49_Subregion == region)), 'GILRHO'])
        df_pred[((df_pred$Years == year + 1) & (df_pred$M49_Subregion == region)), 'mean_subregion_prev1'] <- mean
        df_pred[((df_pred$Years == year + 2) & (df_pred$M49_Subregion == region)), 'mean_subregion_prev2'] <- mean
    }

    # Update GILRHO
    for (region in unique(df_pred$ISO3166)) {
        mean <- mean(df_pred[((df_pred$Years == year) & (df_pred$ISO3166 == region)), 'GILRHO'])
        df_pred[((df_pred$Years == year + 1) & (df_pred$ISO3166 == region)), 'gilrho_prev1'] <- mean
        df_pred[((df_pred$Years == year + 2) & (df_pred$ISO3166 == region)), 'gilrho_prev2'] <- mean
        df_pred[((df_pred$Years == year + 3) & (df_pred$ISO3166 == region)), 'gilrho_prev3'] <- mean
        df_pred[((df_pred$Years == year + 4) & (df_pred$ISO3166 == region)), 'gilrho_prev4'] <- mean
        df_pred[((df_pred$Years == year + 5) & (df_pred$ISO3166 == region)), 'gilrho_prev5'] <- mean
    }

    return(df_pred)
}

csc_predict <- function(df_pred, csc) {
    for (year in 2017:2050) {
        df_pred$mean_subregion_diff <- (df_pred$mean_subregion_prev1 - df_pred$mean_subregion_prev2)
        df_pred$mean_region_diff <- (df_pred$mean_region_prev1 - df_pred$mean_region_prev2)
        df_year        <- subset(df_pred, df_pred$Years == year)
        # Compute predictions
        predictions    <- sapply(predict(csc, df_year), stand)
        # Write predictions
        df_pred$GILRHO[df_pred$Years == year] <- predictions
        df_pred <- update_means(df_pred, year)
    }
    return(df_pred)
}

display_cloropleth <- function(df_train, df_pred) {
    df_pred$Years                             <-as.integer(df_pred$Years)
    df_pred$ISO3166                           <-as.character(df_pred$ISO3166)
    df_pred$GILRHO                            <- as.numeric(df_pred$GILRHO)
    df_pred[(df_pred$GILRHO > 8), 'GILRHO']   <- 8
    df_pred[(df_pred$GILRHO < 0.1), 'GILRHO'] <- 0

    df_todo        <- rbind(df_train, df_pred)
    df_todo$GILRHO <- as.numeric(df_todo$GILRHO)
    df_todo$ISO3166 <- as.character(df_todo$ISO3166)
    ichoropleth(GILRHO ~ ISO3166, data = df_todo, labels = FALSE, ncuts=100, animate="Years", map='world', pal = "Spectral")

}

write_predictions <- function(df_train, df_pred, filename) {
    df_pred$Years                             <-as.integer(df_pred$Years)
    df_pred$ISO3166                           <-as.character(df_pred$ISO3166)
    df_pred$GILRHO                            <- as.numeric(df_pred$GILRHO)
    df_pred[(df_pred$GILRHO > 8), 'GILRHO']   <- 8
    df_pred[(df_pred$GILRHO < 0.1), 'GILRHO'] <- 0
  
    df_todo        <- rbind(df_train, df_pred)
    df_todo$GILRHO <- as.numeric(df_todo$GILRHO)
    df_todo$ISO3166 <- as.character(df_todo$ISO3166)
    
    write.csv(df_todo[,c("ISO3166","GILRHO","Years")], filename)
    
    return(df_todo[,c("ISO3166","GILRHO","Years")])
}

load_data_map <- function(fknn, frf, fann) {
    columnas <- c("names", "Years", "GILRHO")
    dataknn <- read.csv(file = fknn, header = TRUE)
    dataknn$names <- iso.expand(lapply(dataknn$ISO3166, as.character))

    datarf <- read.csv(file = frf, header = TRUE)
    datarf$names <- iso.expand(lapply(datarf$ISO3166, as.character))

    dataann <- read.csv(file = fann, header = TRUE)
    dataann$names <- iso.expand(lapply(dataann$ISO3166, as.character))
    
    mapWorld = map('world', fill = TRUE, col = 1:10, plot = FALSE)
    df_maps <- data.frame(mapWorld$names)
    df_maps$mapWorld.names <- as.character(df_maps$mapWorld.names)

    list_knn <- list()
    list_rf <- list()
    list_ann <- list()
    for (year in 1960:2050) {
        df_year_knn <- subset(dataknn, dataknn$Years == year)
        df_year_rf <- subset(datarf, datarf$Years == year)
        df_year_ann <- subset(dataann, dataann$Years == year)

        df_maps$GILRHOknn <- NA
        df_maps$GILRHOrf  <- NA
        df_maps$GILRHOann <- NA
        
        for(i in 1:nrow(df_year_knn)) {
            df_maps$GILRHOknn[grepl(df_year_knn$names[i], df_maps$mapWorld.names, perl = TRUE)] <- df_year_knn$GILRHO[i]
            df_maps$GILRHOrf[grepl(df_year_rf$names[i], df_maps$mapWorld.names, perl = TRUE)] <- df_year_rf$GILRHO[i]
            df_maps$GILRHOann[grepl(df_year_ann$names[i], df_maps$mapWorld.names, perl = TRUE)] <- df_year_ann$GILRHO[i]

        }
        df_maps$GILRHOknn <- as.numeric(as.character(df_maps$GILRHOknn))
        df_maps$GILRHOrf  <- as.numeric(as.character(df_maps$GILRHOrf))
        df_maps$GILRHOann <- as.numeric(as.character(df_maps$GILRHOann))
        list_knn[[year]]  <- df_maps$GILRHOknn
        list_rf[[year]]   <- df_maps$GILRHOrf
        list_ann[[year]]  <- df_maps$GILRHOann
    }
    mapWorld$GILRHOknn <- list_knn
    mapWorld$GILRHOrf  <- list_rf
    mapWorld$GILRHOann <- list_ann
    return(mapWorld)
}

display_map <- function(map, year) {

    # Subset year
    map$knn <- map$GILRHOknn[[year]]
    map$rf  <- map$GILRHOrf[[year]]
    map$ann <- map$GILRHOann[[year]]

    # Create palettes
    palknn <- colorBin("RdYlGn", map$GILRHOknn[[2016]], 8, pretty = FALSE, alpha = FALSE)
    palrf  <- colorBin("RdYlGn", map$GILRHOrf[[2016]], 8, pretty = FALSE, alpha = FALSE)
    palann <- colorBin("RdYlGn", map$GILRHOann[[2016]], 8, pretty = FALSE, alpha = FALSE)

    # Create popup
    gilrho_popup <- paste0("<center><strong>", map$name, "</strong></center>",
                      "<strong>GILRHO (kNN): </strong>", 
                      format(round(map$knn,2), nsmall = 1), 
                      "<br><strong>GILRHO (Random Forest): </strong>", 
                      format(round(map$rf,2), nsmall = 1), 
                      "<br><strong>GILRHO (Neural Network): </strong>", 
                      format(round(map$ann,2), nsmall = 1)
                      )
    
    # Create leaflet
    if (year > 2016) {
        leaflet(data = map) %>%
            addTiles() %>%
        #addProviderTiles("CartoDB.Positron") %>%
        #addProviderTiles("Stamen.Watercolor") %>%
            addPolygons(stroke = FALSE,
                        fillColor = ~palknn(knn), opacity=1, fillOpacity=0.7, group = "kNN", popup = gilrho_popup) %>%
            addPolygons(stroke = FALSE,
                        fillColor = ~palrf(rf), opacity=1, fillOpacity=0.7, group = "Random Forest", popup = gilrho_popup) %>%
            addPolygons(stroke = FALSE,
                        fillColor = ~palann(ann), opacity=1, fillOpacity=0.7, group = "Neural Network", popup = gilrho_popup) %>%
            addLegend("bottomright", pal = palann, values = ~ann,
                      title = "GILRHO",
                      opacity = 1) %>%
            addLayersControl(
                baseGroups = c("kNN", "Random Forest", "Neural Network"),
                options = layersControlOptions(collapsed = FALSE)
            )
    } else {
        leaflet(data = map) %>%
            addTiles() %>%
            #addProviderTiles("CartoDB.Positron") %>%
            #addProviderTiles("Stamen.Watercolor") %>%
            addPolygons(stroke = FALSE,
                        fillColor = ~palknn(knn), opacity=1, fillOpacity=0.7, group = "kNN", popup = gilrho_popup) %>%
            addLegend("bottomright", pal = palann, values = ~ann,
                        title = "GILRHO",
                        opacity = 1)
    }
}
