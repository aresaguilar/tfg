######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: func_ann.r                                                   #
# Description: Functions to create and evaluate an artificial neural #
#              network with backpropagation for the LGBT data.       #
# Author: Ares Aguilar Sotos                                         #
######################################################################

ann_create <- function(data_train, hidden) {
    ann<-make_Weka_classifier("weka.classifiers.functions.MultilayerPerceptron")
    ann_csc<-ann(GILRHO~Years + M49_Region + M49_Subregion
                    + gilrho_prev1 + gilrho_prev2 + gilrho_prev3 + gilrho_prev4 + gilrho_prev5
                    + mean_subregion_diff
                    + mean_region_diff
                    - ISO3166
                    - mean_region_prev1 - mean_region_prev2
                    - mean_subregion_prev1 - mean_subregion_prev2
                ,data = data_train
                ,control = Weka_control(H = hidden))
    return(ann_csc)
}

ann_write_errors <- function(data_train, name, start, end) {
    filename <- paste0("output/", name)
    plotname <- paste0("plots/", name, ".png")
    unlink(filename)
    unlink(plotname)
    for(i in start:end) {
        ann_csc <- ann_create(data_train, i)
        eval_csc <- evaluate_Weka_classifier(ann_csc, numFolds = 5)
        write(c(i, as.character(eval_csc$details["relativeAbsoluteError"])),
              file=filename,
              ncolumns=2,
              append=TRUE,
              sep=' ')
    }
    errors <- read.csv(filename, sep = " ", header = FALSE)
    ggplot(data=errors, aes(x=errors$V1,y=errors$V2)) +
        geom_point(color="red") +
        geom_line(color="blue") +
        xlab("Neurons in hidden layer") +
        ylab("Absolute Error (%)")
    ggsave(filename=plotname)
}
