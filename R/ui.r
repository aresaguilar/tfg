navbarPage("LGBT Rights Timeseries", id="nav",
           tabPanel("Mapa", value = "mapa",
                    div(class="outer",
                        tags$head(
                          # Include our custom CSS
                          includeCSS("styles.css")
                        ),
                      leafletOutput("mymap", width="100%", height="100%"),
                      absolutePanel(id = "controls", class = "panel panel-default", fixed = TRUE,
                                    draggable = TRUE, top = 60, left = "auto", right = 20, bottom = "auto",
                                    width = 330, height = "auto",
                                    
                                    h2("GILRHO explorer"),
                                    sliderInput("year", "Año: ", min = 1960, max = 2050,
                                                value = 1960, step = 1,
                                                animate = animationOptions(interval = 3000, loop = FALSE))
                      )
                    )
           ),
           tabPanel("Cluster", value = "cluster",
                    div(class="outer",
                        tags$head(
                          # Include our custom CSS
                          includeCSS("styles.css")
                        ),
                        leafletOutput("clustermap", width="100%", height="100%"),
                        absolutePanel(id = "clustercontrols", class = "panel panel-default", fixed = TRUE,
                                      draggable = TRUE, top = 60, left = "auto", right = 20, bottom = "auto",
                                      width = 330, height = "auto",
                                      
                                      h2("GILRHO clusters"),
                                      sliderInput("clusters", "Nº de clusters: ", min = 1, max = 10,
                                                  value = 7, step = 1,
                                                  animate = animationOptions(interval = 3000, loop = FALSE))
                        )
                    )
           ),
           tabPanel("Datos", value = "datos",
                    dataTableOutput('mytable'))
)