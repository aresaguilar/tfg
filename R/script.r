######################################################################
# Project: LGBT Rights Timeseries                                    #
# File: script.r                                                     #
# Description: Script calling every other script :)                  #
# Author: Ares Aguilar Sotos                                         #
######################################################################

setwd("~/TFG/R")
source("init.r")          # Load/Install required packages
source("prep.r")          # Open and prepare data
source("func.r")          # Load generic functions
source("func_knn.r")      # Load k-NN functions
source("func_rf.r")       # Load random forest functions
source("func_ann.r")      # Load neural network functions
source("func_cluster.r")  # Load clustering functions

# ESCENARIO OPTIMISTA
# stand <- function(x) {
#     if (x < 0)
#         return(0)
#     if (x > 8)
#         return (8)
#     if ((x > 3) && (x < 6)) {
#         return(floor(x +1.5))
#     }
#     if (x %% 1 < 0.3)
#         return (x)
#     return(ceiling(x))
# }

# ESCENARIO PESIMISTA
# stand <- function(x) {
#     if (x < 0.5)
#         return(0)
#     if (x > 8)
#         return (8)
#     if (x %% 1 < 0.1)
#         return (floor(x - 0.5))
#     if (x %% 1 < 0.7)
#         return (floor(x))
#     return(x)
# }

# Compute errors
knn_write_errors(df_train, "errors_knn", 1, 10)
rf_write_errors(df_train, "errors_rf", 1, 20)
ann_write_errors(df_train, "errors_ann", 1, 10)

# Create classifiers
csc_knn     <- knn_create(df_train, 3)
csc_rf      <- rf_create(df_train, 8)
csc_ann     <- ann_create(df_train, 5)
csc_cluster <- cluster_create(df_train, "DTW")
cluster_plot(csc_cluster,"plots/cluster.png")

# Get predictions
pred_knn     <- csc_predict(df_pred, csc_knn)
pred_rf      <- csc_predict(df_pred, csc_rf)
pred_ann     <- csc_predict(df_pred, csc_ann)
pred_cluster <- cluster_predict(csc_cluster, 1, 10)

# Write predictions to file
write_predictions(df_train_bak, pred_knn, "output/pred_knn.csv")
write_predictions(df_train_bak, pred_rf, "output/pred_rf.csv")
write_predictions(df_train_bak, pred_ann, "output/pred_ann.csv")
write.csv(pred_cluster, "output/pred_cluster.csv", row.names = FALSE)

map <- load_data_map("output/pred_knn.csv", "output/pred_rf.csv", "output/pred_ann.csv")
# map <- load_data_map("output/pred_knn_opt.csv", "output/pred_rf_opt.csv", "output/pred_ann_opt.csv")
# map <- load_data_map("output/pred_knn_pes.csv", "output/pred_rf_pes.csv", "output/pred_ann_pes.csv")
mapClust <- load_cluster_map("output/pred_cluster.csv")
display_map(map, 2045)
display_cluster_map(mapClust, 6)
